Special thanks:
BigHugeThesaurus for synonyms:Thesaurus service provided by [words.bighugelabs.com]
[flaticon.com] for icons: 
* designed by Freepik from Flaticon [freepik.com], 
* designed by Roundicons from Flaticon [roundicons.com], 
* designed by MadeByOliver from Flaticon
VOICE RSS - free online text to speech service [voicerss.org]
Реализовано с помощью сервиса Яндекс Словарь [tech.yandex.ru/dictionary]
XDXF - XML Dictionary Exchange Format [github.com/soshial/xdxf_makedict]