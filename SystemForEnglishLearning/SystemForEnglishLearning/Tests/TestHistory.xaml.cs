﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SystemForEnglishLearning.Tests
{
    /// <summary>
    /// Interaction logic for TestHistory.xaml
    /// </summary>
    public partial class TestHistory : Window, ITestHistoryView
    {
        public TestHistory()
        {
            InitializeComponent();
        }

        TestHistory(double left, double top)
            : this()
        {
            this.Left = left;
            this.Top = top;
        }

        public TestHistory(int userId, double left, double top)
            : this(left, top)
        {
            new TestHistoryPresenter(this, userId);
        }

        public TestHistory(int userId)
            : this()
        {
            new TestHistoryPresenter(this, userId);
        }

        public void SetHistoryData(List<TestsHistoryModel> data) {
            dataGrid.DataContext = null;
            dataGrid.DataContext = data;
        }

        public event EventHandler Row_DoubleClick = null;
        private void Row_DoubleClick_1(object sender, MouseButtonEventArgs e)
        {
            Row_DoubleClick(sender, e);
            //DataRowView drv = (DataRowView)dataGrid.SelectedItem;
            //int id = Convert.ToInt32(drv["TestId"]);
            //string quest = drv["Questions"].ToString();
            //string answers = drv["Answers"].ToString();
            //MessageBox.Show(id + quest + answers);
        }

        public TestsHistoryModel GetSelectedRow(object sender) {
            return (sender as DataGridRow).Item as TestsHistoryModel;
        }
        //public int GetChoosen

    }
}
