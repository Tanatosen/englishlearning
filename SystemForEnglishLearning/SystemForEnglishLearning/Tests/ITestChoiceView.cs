﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    interface ITestChoiceView
    {
        event EventHandler Item_DoubleClick;
        event EventHandler CreateTest_Click;
        void SetData(List<TestsModel> data, bool userTree);
        TestsModel GetChoosenModel(object sender);
        List<TestsModel> GetGroupModel(object sender);
    }
}
