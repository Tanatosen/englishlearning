﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    public class QuestionsModel
    {

        public QuestionsModel(int id, int testId, string text, byte[] image) {
            Id = id;
            TestId = testId;
            Text = text;
            Image = image;
            answers = new List<AnswersModel>();
        }

        string connectionString = "Data Source=|DataDirectory|\\EnglishLearning.sdf";

        List<AnswersModel> answers;

        public List<AnswersModel> Answers
        {
            get
            {
                if (answers.Count == 0)
                {
                    answers = CreateAnswers(Id);
                    answers = Shuffle.ShuffleList(answers);
                    return answers;
                }
                else
                {
                    return answers;
                }
            }
            private set{
                answers = value;
            }
        }

        List<AnswersModel> CreateAnswers(int questionId)
        {
            List<AnswersModel> result = new List<AnswersModel>();
            using (SqlCeConnection connection = new SqlCeConnection(connectionString))
            {
                connection.Open();
                using (SqlCeCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT AnswerId, QuestionId, AnswerText, AnswerImage, Rightness FROM Answer WHERE QuestionId=@id";
                    cmd.Parameters.AddWithValue("@id", questionId);
                    SqlCeDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        int id = Convert.ToInt32(dr["AnswerId"]);
                        string text = dr["AnswerText"].ToString();
                        byte[] image = dr["AnswerImage"] as byte[];
                        bool rightness = Convert.ToBoolean(dr["Rightness"]);
                        result.Add(new AnswersModel(id, questionId, text, image, rightness));
                    }
                }
            }
            return result;
        }

        public int Id
        {
            get;
            private set;
        }

        public int TestId
        {
            get;
            private set;
        }

        public string Text
        {
            get;
            private set;
        }

        public byte[] Image
        {
            get;
            private set;
        }
    }
}
