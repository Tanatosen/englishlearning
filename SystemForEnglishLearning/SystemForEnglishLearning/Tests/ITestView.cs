﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    interface ITestView : IBorderView
    {
        event EventHandler Border_MouseLeftButtonDown;
        event EventHandler NextBorder_MouseLeftButtonDown;
        void SetQuestion(QuestionsModel quest);
        void SetAllQuestions(List<QuestionsModel> questions);
        void SetAnswers(List<AnswersModel> answers);
        void SetNextButton(bool end, int questionId);
        void SetCompleteColor(int id, bool answered);
        int GetIndex(object sender);
        List<int> GetCheckedAnswers(int answerCount);
        void ClearGrid();
    }
}
