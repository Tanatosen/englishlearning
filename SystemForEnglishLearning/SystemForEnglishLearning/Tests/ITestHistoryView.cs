﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    interface ITestHistoryView
    {
        void SetHistoryData(List<TestsHistoryModel> data);
        TestsHistoryModel GetSelectedRow(object sender);
        event EventHandler Row_DoubleClick;
    }
}
