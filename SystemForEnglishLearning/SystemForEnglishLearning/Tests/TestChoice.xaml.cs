﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SystemForEnglishLearning.Tests
{
    /// <summary>
    /// Interaction logic for TestChoice.xaml
    /// </summary>
    public partial class TestChoice : Window, ITestChoiceView
    {
        public TestChoice()
        {
            InitializeComponent();
            new TestChoicePresenter(this, 2);
        }

        TestChoice(double left, double top)
            : this()
        {
            this.Left = left;
            this.Top = top;
        }

        public TestChoice(int userId, double left, double top)
            : this(left, top)
        {
            new TestChoicePresenter(this, userId);
        }

        public TestChoice(int userId)
        {
            new TestChoicePresenter(this, userId);
        }

        public void SetData(List<TestsModel> data, bool userTree)
        {

            List<TestGroupModel> root = new List<TestGroupModel>();
            data.Sort((w1, w2) => w1.Type.CompareTo(w2.Type));
            string type = "";
            foreach (TestsModel el in data)
            {
                if (el.Type != type)
                {
                    type = el.Type;
                    root.Add(new TestGroupModel());
                    root[root.Count - 1].Type = type;
                }
                root[root.Count - 1].Items.Add(el);
            }
            if (userTree) myTree.ItemsSource = root;
            else Tree.ItemsSource = root;
        }

        public event EventHandler Item_DoubleClick = null;
        protected void HandleDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Item_DoubleClick(sender, e);
        }

        public TestsModel GetChoosenModel(object sender)
        {
            TestsModel mod = (sender as TreeViewItem).Header as TestsModel;
            if (mod != null)
            {
                return mod;
            }
            return null;
        }

        public List<TestsModel> GetGroupModel(object sender)
        {
            TestGroupModel group = (sender as TreeViewItem).Header as TestGroupModel;
            return new List<TestsModel>(group.Items);
        }

        public event EventHandler CreateTest_Click = null;
        private void CreateButton_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            CreateTest_Click(sender, e);
            //Create add test window
        }
    }
}
