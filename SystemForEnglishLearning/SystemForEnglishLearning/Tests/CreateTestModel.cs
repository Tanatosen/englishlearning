﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    class CreateTestModel
    {
        int userId;

        public TestsModel Test
        {
            get;
            set;
        }

        public CreateTestModel(int userId) {
            this.userId = userId;
        }
    }
}
