﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    class TestModel
    {
        string connectionString = "Data Source=|DataDirectory|\\EnglishLearning.sdf";
        List<QuestionsModel> questions;
        List<AnswersModel> answers;
        int constId = 1;
        int testId;

        public TestModel(int testId)
        {
            this.testId = testId;
            questions = new List<QuestionsModel>();
            answers = new List<AnswersModel>();
            Questions = CreateQuestions(testId);
            Shuffle(questions);
        }

        public List<QuestionsModel> Questions
        {
            get
            {
                if (questions != null && questions.Count != 0)
                {
                    return questions;
                }
                else
                {
                    return null;
                }
            }
            private set
            {
                questions = value;
            }
        }

        public List<AnswersModel> GetAnswers()
        {
            return answers;
        }

        public void SetAnswers(int questionId)
        {
            answers = CreateAnswers(questionId);
        }

        List<QuestionsModel> CreateQuestions(int testId)
        {
            List<QuestionsModel> result = new List<QuestionsModel>();
            using (SqlCeConnection connection = new SqlCeConnection(connectionString))
            {
                connection.Open();
                using (SqlCeCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT QuestionId, QuestText, QuestImage FROM Question WHERE TestId=@id";
                    cmd.Parameters.AddWithValue("@id", testId);
                    SqlCeDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        int id = Convert.ToInt32(dr["QuestionId"]);
                        string text = dr["QuestText"].ToString();
                        byte[] image = dr["QuestImage"] as byte[];
                        result.Add(new QuestionsModel(id, testId, text, image));
                    }
                }
            }
            return result;
        }

        List<AnswersModel> CreateAnswers(int questionId)
        {
            List<AnswersModel> result = new List<AnswersModel>();
            using (SqlCeConnection connection = new SqlCeConnection(connectionString))
            {
                connection.Open();
                using (SqlCeCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT AnswerId, QuestionId, AnswerText, AnswerImage, Rightness FROM Answer WHERE QuestionId=@id";
                    cmd.Parameters.AddWithValue("@id", testId);
                    SqlCeDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        int id = Convert.ToInt32(dr["AnswerId"]);
                        string text = dr["AnswerText"].ToString();
                        byte[] image = dr["AnswerImage"] as byte[];
                        bool rightness = Convert.ToBoolean(dr["Rightness"]);
                        result.Add(new AnswersModel(id, questionId, text, image, rightness));
                    }
                }
            }
            return result;
        }

        protected List<T> Shuffle<T>(List<T> list)
        {
            Random rand = new Random();
            for (int i = 0; i < list.Count; i++)
            {
                Swap(i, rand.Next(i, list.Count), list);
            }
            return list;
        }

        protected void Swap<T>(int i, int j, List<T> list)
        {
            T temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }

    }
}
