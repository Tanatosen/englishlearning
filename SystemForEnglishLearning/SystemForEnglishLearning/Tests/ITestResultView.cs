﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    interface ITestResultView : IBorderView
    {
        void SetPercent(float percent);
        void SetMainData(TestsModel test);
        void ChangeQuestionsSize(List<QuestionsModel> questions);
        event EventHandler Border_MouseLeftButtonDown;
        event EventHandler Window_StateChanged;
    }
}
