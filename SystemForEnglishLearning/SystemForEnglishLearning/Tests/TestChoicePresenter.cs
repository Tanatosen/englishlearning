﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SystemForEnglishLearning.Tests
{
    class TestChoicePresenter
    {
        ITestChoiceView window = null;
        TestChoiceModel model = null;
        List<TestsModel> lections;
        int userId;

        public TestChoicePresenter(ITestChoiceView win, int userId) {
            model = new TestChoiceModel(userId);
            window = win;
            if (model.Tests.Count == 0) {
                MessageBox.Show("Тесты отсутствуют");
                (win as Window).Close();
            }
            window.Item_DoubleClick += window_Item_DoubleClick;
            window.CreateTest_Click += window_CreateTest_Click;
            win.SetData(model.Tests,false);
            win.SetData(model.UserTests, true);
            this.userId = userId;
            lections = new List<TestsModel>();
        }

        void window_CreateTest_Click(object sender, EventArgs e)
        {
            Window win = window as Window;
            CreateTest create = new CreateTest(userId, win.Left, win.Top, win.WindowState);
            create.Show();
            win.Close();
        }

        void window_Item_DoubleClick(object sender, EventArgs e)
        {
            TestsModel mod = window.GetChoosenModel(sender);
            if (mod != null)
            {
                Window win = window as Window;
                Test testWindow = new Test(userId, mod, win.Left, win.Top, win.WindowState);
                testWindow.ShowDialog();
            }
        }
    }
}
