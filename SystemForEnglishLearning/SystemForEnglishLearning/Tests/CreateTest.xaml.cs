﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SystemForEnglishLearning.Tests
{
    /// <summary>
    /// Interaction logic for CreateTest.xaml
    /// </summary>
    public partial class CreateTest : Window, ICreateTestView
    {
        int fontSize;

        CreateTest(WindowState state)
        {
            InitializeComponent();
            this.WindowState = state;
            //fontSize = WindowStateCheck();
        }

        CreateTest(double left, double top, WindowState state)
            : this(state)
        {
            this.Left = left;
            this.Top = top;
        }

        public CreateTest(int userId, double left, double top, WindowState state)
            : this(left, top, state)
        {
            new CreateTestPresenter(this, userId);
        }

        public CreateTest(int userId, WindowState state)
            : this(state)
        {
            new CreateTestPresenter(this, userId);
        }

        void CreateMainContent()
        {
        }

        private void AddQuest_Click_1(object sender, RoutedEventArgs e)
        {
            Button bt = sender as Button;
            TextBox tb = new TextBox();
            sp_Questions.Children.Add(tb);
            sp_Questions.Children.Remove(bt);
            sp_Questions.Children.Add(bt);

        }
    }
}
