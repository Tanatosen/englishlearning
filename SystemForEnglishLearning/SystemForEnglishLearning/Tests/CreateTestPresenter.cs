﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Tests
{
    class CreateTestPresenter
    {
        ICreateTestView window = null;
        CreateTestModel model = null;

        public CreateTestPresenter(ICreateTestView win, int userId) {
            window = win;

            model = new CreateTestModel(userId);
        }
    }
}
