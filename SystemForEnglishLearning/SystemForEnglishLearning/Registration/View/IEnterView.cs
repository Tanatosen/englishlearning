﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Register
{
    interface IEnterView
    {
        event EventHandler EnterButton_Click;
        event EventHandler RegisterButton_Click;
        string LoginText { get; }
        string PasswordText { get; }
    }
}
