﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SystemForEnglishLearning.Register
{
    class RegisterPresenter
    {
        IRegistrationView win = null;
        EnterRegisterModel model = null;

        public RegisterPresenter(IRegistrationView registerWindow)
        {
            model = new EnterRegisterModel();
            win = registerWindow;
            win.RegisterButton_Click += new EventHandler(registerWindow_RegisterButtonClick);
        }

        void registerWindow_RegisterButtonClick(object sender, EventArgs e) {
            if (model.Validate(win.LoginText, win.PasswordText) && model.ValidateLoginUnique(win.LoginText))
            {
                if (model.AddUser(win.LoginText, win.PasswordText))
                {
                    MessageBox.Show("Вас успешно зарагестрировано!");
                    (win as Window).Close();
                }
            }
            else {
                MessageBox.Show("Логин должен быть уникальным, логин и пароль минимум 5 символов");
                //win.Login.ToolTip = "Логин должен быть уникален и содержать минимум 5 символов";
                //win.Login.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.PaleVioletRed);
                //win.Password.ToolTip = "Пароль должен содержать минимум 5 символов";
                //win.Password.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.PaleVioletRed);
            }
        }
    }
}
