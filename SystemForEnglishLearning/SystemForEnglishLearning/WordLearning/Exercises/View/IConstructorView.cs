﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.WordLearning.Exercises
{
    interface IConstructorView : IBorderView
    {
        event EventHandler Variant_MouseLeftButtonDown;
        event EventHandler Next_MouseLeftButtonDown;
        event EventHandler Complete_MouseLeftButtonDown;
        event EventHandler Window_Closing;
        string GetAnswer();
        int GetEmptyIndex();
    }
}
