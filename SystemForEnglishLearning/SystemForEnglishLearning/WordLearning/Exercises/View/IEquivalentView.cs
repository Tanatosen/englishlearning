﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.WordLearning.Exercises
{
    interface IEquivalentView : IBorderView
    {
        event EventHandler Image_MouseLeftButtonDown;
        event EventHandler MediaEnded;
        event EventHandler Variant_MouseLeftButtonDown;
        event EventHandler Next_MouseLeftButtonDown;
        event EventHandler Complete_MouseLeftButtonDown;
        event EventHandler Window_Closing;
    }
}
