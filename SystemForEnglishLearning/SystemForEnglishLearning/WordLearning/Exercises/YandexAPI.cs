﻿using System;
using System.Collections.Generic;
#if DEBUG
using System.Diagnostics;
#endif
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SystemForEnglishLearning.WordLearning.Exercises
{
    class YandexAPI
    {

        int synonymNumber = 5;
        //public string GetWordSynonym(string word) {

        //}

        public List<string> GetAllSynonyms(string word)
        {
            string synonyms = null;
            try
            {
                synonyms = UploadSynonyms(word);
                return Parse(synonyms);
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine(ex.Message);
#endif
                return new List<string>();
            }
        }

        List<string> Parse(string xml)
        {
            List<string> synonyms = new List<string>();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeList elemlist = doc.GetElementsByTagName("syn");
            int endIndex;
            if (elemlist.Count <= synonymNumber) { endIndex = elemlist.Count; }
            else { endIndex = synonymNumber; }
            for (int i = 0; i < endIndex; i++)
            {
                XmlNodeList text = elemlist[i].ChildNodes;
                synonyms.Add(text[0].InnerText);
            }

            return synonyms;
        }

        string UploadSynonyms(string word)
        {
            using (var client = new System.Net.WebClient())
            {
                return client.DownloadString("https://dictionary.yandex.net/api/v1/dicservice/lookup?key=dict.1.1.20170423T233402Z.41456fdb00fc6d24.9f39b3d48c80cd270a8a1270056119e899777bc2&lang=en-en&text=" + word);
            }
        }
    }
}
