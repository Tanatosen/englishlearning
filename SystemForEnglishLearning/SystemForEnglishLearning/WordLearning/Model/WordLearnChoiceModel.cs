﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.WordLearning
{
    class WordLearnChoiceModel
    {
        readonly string connectionString = "Data Source=|DataDirectory|\\EnglishLearning.sdf";
        int userId;

        public WordLearnChoiceModel(int userId) {
            this.userId = userId;
        }

        /// <summary>
        /// Return number of words for specific user, if number of words less that need for exercise then you can generate some error
        /// </summary>
        /// <param name="type">type of exercise: "translate", "equivalent", "constructor", "synonym" etc </param>
        /// <returns>return -1 if you type of exercise wrong</returns>
        public int GetCount(string type) {
            string query = "SELECT COUNT(WordId) FROM [LearningWord] WHERE ([LearnPercent] < 100) AND [UserId] = @user";
            switch(type){
                case ("translate"): {
                    return SampleCount(query);
                }
                case ("equivalent"): {
                    return SampleCount(query);
                }
                case ("synonym"): {
                    return SampleCount(query);
                }
                case ("listening"): {
                    query = "SELECT COUNT(w.WordId) FROM [Word] AS w LEFT JOIN [LearningWord] AS lw ON lw.WordId = w.WordId WHERE lw.UserId = @user AND lw.LearnPercent < 100 AND w.Voice IS NOT NULL";
                    return SampleCount(query);
                }
                case ("constructor"): {
                    return SampleCount(query);
                }
                default:{
                    return -1;
                }
            }
        }

        //type of exercise: "translate", "equivalent"
        public int SampleCount(string query) {
            int result;
            using (SqlCeConnection connection = new SqlCeConnection(connectionString))
            {
                connection.Open();
                using (SqlCeCommand command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.Parameters.AddWithValue("@user", userId);
                    result = (int)command.ExecuteScalar();
                }
                connection.Close();
                return result;
            }
        }

        //public int SynonymCount() {
        //    int result;
        //    using (SqlCeConnection connection = new SqlCeConnection(connectionString))
        //    {
        //        connection.Open();
        //        using (SqlCeCommand command = connection.CreateCommand())
        //        {
        //            command.CommandText = "SELECT COUNT(syn.SynonymId) FROM [Synonym] AS syn LEFT JOIN [LearningWord] AS lw ON syn.FirstWordId=lw.WordId WHERE (lw.LearnPercent < 100) AND lw.UserId = @user";
        //            command.Parameters.AddWithValue("@user", userId);
        //            result = (int)command.ExecuteScalar();
        //        }
        //        connection.Close();
        //        return result;
        //    }
        //}

    }
}
