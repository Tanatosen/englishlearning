﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.WordLearning
{
    class WordModel : ICloneable
    {
        public WordModel(int id, string word) {
            WordId = id;
            Word = word;
        }

        public WordModel(int id, string word, string translate, bool inLearning) {
            WordId = id;
            Word = word;
            Translate = translate;
            OnLearning = inLearning;
        }

        public WordModel(int id, string word, string translate, string partOfSpeech, string transcription, byte[] voice, bool inLearning) {
            WordId = id;
            Word = word;
            Translate = translate;
            PartOfSpeech = partOfSpeech;
            Transcription = transcription;
            Voice = voice;
            OnLearning = inLearning;
        }

        public int WordId
        {
            get;
            private set;
        }

        public string Word
        {
            get;
            private set;
        }

        public string Translate
        {
            get;
            private set;
        }

        public string PartOfSpeech
        {
            get;
            private set;
        }

        public string Transcription
        {
            get;
            private set;
        }

        public byte[] Voice
        {
            get;
            set;
        }

        public bool OnLearning
        {
            get;
            set;
        }

        public object Clone()
        {
            return new WordModel(WordId, Word, Translate, PartOfSpeech, Transcription, Voice, OnLearning) as object;
        }
    }
}
