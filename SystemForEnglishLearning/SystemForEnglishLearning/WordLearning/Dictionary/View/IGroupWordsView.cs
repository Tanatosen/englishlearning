﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.WordLearning.Dictionary
{
    interface IGroupWordsView : IBorderView
    {
        event EventHandler Button_MouseLeftButtonUp;
        event EventHandler Btn_Add_Click;
        event EventHandler Btn_Exit_Click;
        event EventHandler Btn_Search_Click;
        event EventHandler ChBx_Checked;
        void SetData<T>(List<T> data);
        void EnablePrev(bool enable);
        void EnableNext(bool enable);
        /// <summary>
        /// 
        /// </summary>
        /// <returns>English word string with index 0, translate string with index 1</returns>
        string[] GetSearchText();
        bool GetCheckAll();
    }
}
