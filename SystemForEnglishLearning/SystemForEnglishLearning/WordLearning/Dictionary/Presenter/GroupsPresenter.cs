﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace SystemForEnglishLearning.WordLearning.Dictionary
{
    class GroupsPresenter
    {
        IGroupsView win = null;
        GroupsModel model = null;
        BorderPresenter border;
        int userId;

        public GroupsPresenter(IGroupsView GroupsWindow, int userId)
        {
            this.userId = userId;
            model = new GroupsModel();
            win = GroupsWindow;
            border = new BorderPresenter(win);
            win.Border_MouseLeftButtonDown += new EventHandler(BorderMouseLeftClick);
            win.Grid_MouseRightButtonDown += new EventHandler(GridMouseRightClick);
            //GenerateContent();
            List<GroupModel> constGroups = model.Groups;
            win.GenerateContent(constGroups);
        }

        void BorderMouseLeftClick(object sender, EventArgs e)
        {
            FrameworkElement bord = sender as FrameworkElement;
            //Border bord = sender as Border;
            //bord.Tag
            GroupWords childWin = new GroupWords(userId, Convert.ToInt32(bord.Tag)); //TO DO: don`t forget change userID
            childWin.WindowState = (win as Window).WindowState;
            childWin.ShowDialog();
        }

        void GridMouseRightClick(object sender, EventArgs e)
        {
            Window window = win as Window;
            WordLearningChoice parentWin = new WordLearningChoice(userId, window.Left, window.Top);
            parentWin.WindowState = window.WindowState;
            parentWin.Show();
            window.Close();
        }

        //void GenerateContent() {
        //    List<GroupModel> constGroups = model.Groups;
        //    foreach (GroupModel el in constGroups) {
        //        Border bord = new Border();
        //        bord.Style = (win as Window).FindResource("BorderStyle") as Style;
        //        bord.Name = "Border" + el.Group;
        //        bord.Tag = el.Group;
        //        Grid grid = CreateGrid(2, 5, GridUnitType.Auto, GridUnitType.Auto);
        //        grid.MinWidth = 188;
        //        Viewbox vB = CreateViewBox(0, 0, 2, 1);
        //        Label lb = CreateLabel(el.Name, 16);
        //        vB.Child = lb;
        //        grid.Children.Add(vB);
        //        string[,] labelVal = new string[,] { {"Слов:", el.WordsCount.ToString()}, {"Сложность:", el.Difficult} };
        //        for (int i = 0; i < 2; i++) {
        //            for (int j = 0; j < 2; j++) {
        //                vB = CreateViewBox(i+1, j, 1, 1);
        //                lb = CreateLabel(labelVal[i,j], 12);
        //                vB.Child = lb;
        //                grid.Children.Add(vB);
        //            }
        //        }
        //        //    vB = CreateViewBox(1, 0, 1, 1);
        //        //lb = CreateLabel("Слов:", 12);
        //        //vB.Child = lb;
        //        //grid.Children.Add(vB);
        //        //vB = CreateViewBox(1, 1, 1, 1);
        //        //lb = CreateLabel(el.WordsCount.ToString(), 12);
        //        //vB.Child = lb;
        //        //grid.Children.Add(vB);
        //        //vB = CreateViewBox(2, 0, 1, 1);
        //        //lb = CreateLabel("Сложность:", 12);
        //        //vB.Child = lb;
        //        //grid.Children.Add(vB);
        //        //vB = CreateViewBox(2, 1, 1, 1);
        //        //lb = CreateLabel(el.Difficult, 12);
        //        //vB.Child = lb;
        //        //grid.Children.Add(vB);
        //        Image img = CreateImage(el.Image, 3, 0, 2, 1, 100);
        //        grid.Children.Add(img);
        //        Button btn = new Button();
        //        btn.Style = (win as Window).FindResource("ButtonStyle") as Style;
        //        SetRowColumnProperties(btn, 4, 0, 2, 1);
        //        grid.Children.Add(btn);
        //        bord.Child = grid;
        //        win.SetContent(bord);
        //        //win.StandardPanel.Children.Add(bord);
        //    }
        //}

        //Grid CreateGrid(int columns, int rows, GridUnitType columnType, GridUnitType rowType)
        //{
        //    Grid grid = new Grid();
        //    for (int i = 0; i < columns; i++)
        //    {
        //        ColumnDefinition col = new ColumnDefinition();
        //        col.Width = new GridLength(0, columnType);
        //        grid.ColumnDefinitions.Add(col);
        //    }
        //    for (int i = 0; i < rows; i++) {
        //        RowDefinition row = new RowDefinition();
        //        row.Height = new GridLength(0, rowType);
        //        grid.RowDefinitions.Add(row);
        //    }
        //    return grid;
        //}

        //Viewbox CreateViewBox(int gridRow, int gridColumn, int gridColumnSpan, int gridRowSpan) {
        //    Viewbox viewBox = new Viewbox();
        //    SetRowColumnProperties(viewBox,gridRow,gridColumn,gridColumnSpan,gridRowSpan);
        //    return viewBox;
        //}

        //Label CreateLabel(string content, int fontSize) {
        //    Label lb = new Label();
        //    lb.Content = content;
        //    lb.FontSize = fontSize;
        //    return lb;
        //}

        //Image CreateImage(byte[] byteImage, int gridRow, int gridColumn, int gridColumnSpan, int gridRowSpan, double height) {
        //    Image img = ConvertByteToImage(byteImage);
        //    SetRowColumnProperties(img, gridRow, gridColumn, gridColumnSpan, gridRowSpan);
        //    img.Height = height;
        //    img.HorizontalAlignment = HorizontalAlignment.Center;
        //    return img;
        //}

        //Image ConvertByteToImage(byte[] byteImage) {
        //    Image img = new Image();
        //    using (var ms = new System.IO.MemoryStream(byteImage))
        //    {
        //        ms.Position = 0;
        //        BitmapImage bi = new BitmapImage();
        //        bi.BeginInit();
        //        bi.CacheOption = BitmapCacheOption.OnLoad;
        //        bi.StreamSource = ms;
        //        bi.EndInit();
        //        img.Source = bi;
        //    }
        //    return img;
        //}

        //void SetRowColumnProperties(UIElement el,int gridRow, int gridColumn, int gridColumnSpan, int gridRowSpan){
        //    Grid.SetRow(el, gridRow);
        //    Grid.SetColumn(el, gridColumn);
        //    Grid.SetColumnSpan(el, gridColumnSpan);
        //    Grid.SetRowSpan(el, gridRowSpan);
        //}

        //<Border Style="{StaticResource BorderStyle}" Margin="0,0,0,0">
        //                    <Grid>
        //                        <Grid.ColumnDefinitions>
        //                            <ColumnDefinition Width="Auto" />
        //                            <ColumnDefinition Width="Auto" />
        //                        </Grid.ColumnDefinitions>
        //                        <Grid.RowDefinitions>
        //                            <RowDefinition Height="Auto" />
        //                            <RowDefinition Height="Auto" />
        //                            <RowDefinition Height="Auto" />
        //                            <RowDefinition Height="Auto" />
        //                        </Grid.RowDefinitions>

        //                        <Viewbox Grid.Row="0" Grid.Column="0" Grid.ColumnSpan="2">
        //                            <Label Content="Название группы" FontSize="16"/>
        //                        </Viewbox>
        //                        <Viewbox Grid.Row="1" Grid.Column="0">
        //                            <Label Content="Количество слов" FontSize="12"/>
        //                        </Viewbox>
        //                        <Viewbox Grid.Row="1" Grid.Column="1">
        //                            <Label Content="Сложность" FontSize="12"/>
        //                        </Viewbox>
        //                        <Image Source="../Resource/dictionary.png" Height="100" Grid.Row="2" Grid.Column="0" Grid.ColumnSpan="2"/>
        //                        <Button Grid.Row="3" Grid.Column="0" Grid.ColumnSpan="2" Height="30" Content="Изучить" FontSize="16"/>
        //                    </Grid>
        //                </Border>

    }
}
