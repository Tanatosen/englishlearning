﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SystemForEnglishLearning.WordLearning.Dictionary
{
    class GroupWordsPresenter
    {
        IGroupWordsView win = null;
        GroupWordsModel model = null;
        List<WordModel> oldWords;
        BorderPresenter border = null;

        public GroupWordsPresenter(IGroupWordsView window, int userId, int groupId)
        {
            win = window;
            border = new BorderPresenter(window);
            model = new GroupWordsModel(userId, groupId);
            //EventManager.RegisterRoutedEvent("ButtonMouseLeftClick", RoutingStrategy.Direct, typeof(RoutedEventHandler), GroupWordsPresenter);
            win.Button_MouseLeftButtonUp += new EventHandler(ButtonMouseLeftClick);
            win.Btn_Add_Click += new EventHandler(ButtonAddClick);
            win.Btn_Exit_Click += new EventHandler(ButtonExitClick);
            win.Btn_Search_Click += win_Btn_Search_Click;
            win.ChBx_Checked += win_ChBx_Checked;
            List<WordModel> words = model.Words;
            UpdateWindow();
            UpdateOldWords();
        }

        void win_ChBx_Checked(object sender, EventArgs e)
        {
            bool value = win.GetCheckAll();
            foreach (WordModel el in model.Words) {
                el.OnLearning = value;
            }
            UpdateWindow();
        }

        void win_Btn_Search_Click(object sender, EventArgs e)
        {
            string[] search = win.GetSearchText();
            model.FindWords(search[0], search[1]);
            UpdateWindow();
            UpdateOldWords();
        }

        void ButtonMouseLeftClick(object sender, EventArgs e)
        {
            FrameworkElement btn = sender as FrameworkElement;
            //Button btn = sender as Button;
            if (btn.Name == "btn_Next")
            {
                if (model.SetPageIndex(true))
                {
                    UpdateWindow();
                    UpdateOldWords();
                }
            }
            else
            {
                if (btn.Name == "btn_Prev")
                {
                    if (model.SetPageIndex(false))
                    {
                        UpdateWindow();
                        UpdateOldWords();
                    }
                }
            }
        }

        void ButtonAddClick(object sender, EventArgs e)
        {
            oldWords.Sort((w1, w2) => w1.WordId.CompareTo(w2.WordId));
            model.Words.Sort((w1, w2) => w1.WordId.CompareTo(w2.WordId));
            int addCount = 0;
            int deleteCount = 0;
            for (int i = 0; i < oldWords.Count; i++)
            {
                if (oldWords[i].OnLearning != model.Words[i].OnLearning)
                {
                    if (model.UpdateRow(model.Words[i].WordId, model.Words[i].OnLearning)) {
                        if (model.Words[i].OnLearning) addCount++;
                        else deleteCount++;
                    }
                }
            }
            UpdateOldWords();
            string message = "";
            if(addCount>0) message+="Добавлено "+addCount+" слов.";
            if(deleteCount>0)message+="Удалено "+deleteCount+" слов.";
            MessageBox.Show(message);
        }

        void ButtonExitClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("Все изменения не будут сохранены, вы действительно хотите выйти?", "Уведомление", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                (win as Window).Close();
            }
        }

        void UpdateWindow()
        {
            if (model.GetMaxPageIndex() <= model.GetPageIndex()) win.EnableNext(false);
            else win.EnableNext(true);
            if (model.GetPageIndex() == 1) win.EnablePrev(false);
            else win.EnablePrev(true);

            //UpdateOldWords();
            win.SetData(model.Words);
            //win.DataGrid.DataContext = model.Words;
            //win.DataStackPanel.Children.RemoveRange(1, win.DataStackPanel.Children.Count - 1);
            //GenerateContent();
        }

        void UpdateOldWords()
        {
            oldWords = new List<WordModel>();
            for (int i = 0; i < model.Words.Count; i++)//win.DataGrid.Items.Count; i++)
            {
                //DataGridRow row = (DataGridRow)win.DataGrid.ItemContainerGenerator
                //                                           .ContainerFromIndex(i);
                oldWords.Add(model.Words[i].Clone() as WordModel);//(row.Item as WordModel).Clone() as WordModel);  //TO DO: do iclonable because this just copy link, this should be in updatewindow method i guess
            }
        }

        //void GenerateContent() {
        //    foreach (WordModel el in model.Words) {
        //        Style bordStyle = win.FindResource("BorderStyle") as Style;
        //        Border bord = DynamicElements.CreateBorder(bordStyle);
        //        Grid grid = DynamicElements.CreateGrid(3, 1, GridUnitType.Star, GridUnitType.Star);
        //        Border inBorder = DynamicElements.CreateBorder(bordStyle,0,0,1,1);
        //        Viewbox vB = DynamicElements.CreateViewBox(30);
        //        TextBlock tB = DynamicElements.CreateTextBlock(el.Word);
        //        tB.Width = 90;
        //        tB.Height = 30;
        //        vB.Child = tB;
        //        vB.Width = 90;
        //        inBorder.Child = vB;
        //        grid.Children.Add(inBorder);
        //        inBorder = DynamicElements.CreateBorder(bordStyle,0,1,1,1);
        //        vB = DynamicElements.CreateViewBox(30);
        //        tB = DynamicElements.CreateTextBlock(el.Translate);
        //        tB.MinWidth = 90;
        //        vB.Child = tB;
        //        inBorder.Child = vB;
        //        grid.Children.Add(inBorder);
        //        inBorder = DynamicElements.CreateBorder(bordStyle,0,2,1,1);
        //        CheckBox cB = DynamicElements.CreateCheckBox(el.OnLearning);
        //        cB.MinWidth = 90;
        //        inBorder.Child = cB;
        //        grid.Children.Add(inBorder);
        //        bord.Child = grid;
        //        win.DataStackPanel.Children.Add(bord);
        //    }
        //}

        //<Border BorderThickness="1" BorderBrush="Azure">
        //                <Grid>
        //                    <Grid.ColumnDefinitions>
        //                        <ColumnDefinition Width="*" />
        //                        <ColumnDefinition Width="*" />
        //                        <ColumnDefinition Width="*" />
        //                    </Grid.ColumnDefinitions>
        //                    <Border BorderThickness="1" BorderBrush="Azure">
        //                        <Viewbox Height="30">
        //                            <TextBlock Text="armchest"/>
        //                        </Viewbox>
        //                    </Border>
        //                    <Border BorderThickness="1" BorderBrush="Azure" Grid.Column="1">
        //                        <Viewbox Height="30">
        //                            <TextBlock Text="плечо"/>
        //                        </Viewbox>
        //                    </Border>
        //                    <Border BorderThickness="1" BorderBrush="Azure" Grid.Column="2">
        //                        <CheckBox HorizontalAlignment="Center" VerticalAlignment="Center"/>
        //                    </Border>
        //                </Grid>
        //            </Border>

    }
}
