﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.WordLearning
{
    interface IWordLearningChoiceView : IBorderView
    {
        event EventHandler Border_MouseLeftButtonDown;
        event EventHandler Grid_MouseRightButtonDown;
    }
}
