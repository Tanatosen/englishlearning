﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForEnglishLearning.Lections
{
    interface ILectionChoiceView : IBorderView
    {
        event EventHandler Item_DoubleClick;
        void SetData(List<LectionsModel> data);
        LectionsModel GetChoosenModel(object sender);
        List<LectionsModel> GetGroupModel(object sender);
    }
}
