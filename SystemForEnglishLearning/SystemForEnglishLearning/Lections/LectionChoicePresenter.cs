﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SystemForEnglishLearning.Lections
{
    class LectionChoicePresenter
    {
        ILectionChoiceView window = null;
        LectionsChoiceModel model = null;
        BorderPresenter border = null;
        List<LectionsModel> lections;
        int userId;

        public LectionChoicePresenter(ILectionChoiceView win, int userId) {
            model = new LectionsChoiceModel();
            window = win;
            border = new BorderPresenter(window);
            if (model.Lections.Count == 0) {
                MessageBox.Show("Лекции отсутствуют");
                (win as Window).Close();
            }
            window.Item_DoubleClick += Item_DoubleClick;
            win.SetData(model.Lections);
            this.userId = userId;
            lections = new List<LectionsModel>();
        }

        void Item_DoubleClick(object sender, EventArgs e)
        {
            LectionsModel mod = window.GetChoosenModel(sender);
            if (mod != null)
            {
                Window win = window as Window;
                Lection lection = new Lection(userId, mod.Id, lections, win.Left, win.Top);
                lection.WindowState = win.WindowState;
                lection.ShowDialog();
            }
            else {
                lections = window.GetGroupModel(sender);
            }
        }

    }
}
