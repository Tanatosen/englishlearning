﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace SystemForEnglishLearning.Lections
{
    interface ILectionView
    {
        void SetDataSideMenu(List<LectionsModel> lectionsList);
        void SetMainData(FixedDocumentSequence lection);
        event EventHandler lectionBtn_Click;
        event EventHandler testBtn_Click;
        int GetNewLectionId(object sender);
        void TestControlEnabled(bool enabled);
        void ContentNullException(string message);
    }
}
