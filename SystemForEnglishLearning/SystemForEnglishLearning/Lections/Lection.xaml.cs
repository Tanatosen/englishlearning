﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SystemForEnglishLearning.WordLearning;

//using System.Windows.Markup;
//using System.IO;
using System.Windows.Xps.Packaging;

namespace SystemForEnglishLearning.Lections
{
    /// <summary>
    /// Interaction logic for Lection.xaml
    /// </summary>
    public partial class Lection : Window, ILectionView
    {
        public Lection()
        {
            InitializeComponent();
            //Document.ViewingMode = FlowDocumentReaderViewingMode.Scroll;
            //FlowDocument doc = new FlowDocument();
            //doc.Background = Brushes.White;
            //Document.Document = doc;
        }

        Lection(double left, double top)
            : this()
        {
            this.Left = left;
            this.Top = top;
        }

        public Lection(int userId, int lectionId, List<LectionsModel> lections, double left, double top)
            : this(left, top)
        {
            new LectionPresenter(this, userId, lectionId, lections);
        }

        public Lection(int userId, int lectionId, List<LectionsModel> lections) : this()
        {
            new LectionPresenter(this, userId, lectionId, lections);
        }

        private void Button_LeftMouseButtonDown(object sender, MouseEventArgs e)
        {
            Document.Background = (sender as Button).Background;
            //Document.Document.Background = (sender as Button).Background;
        }

        public event EventHandler lectionBtn_Click = null;
        private void LectionBtn_LeftMouseButtonDown(object sender, MouseEventArgs e)
        {
            lectionBtn_Click(sender,e);
        }

        public event EventHandler testBtn_Click = null;
        private void TestBtn_LeftMouseButtonDown(object sender, MouseEventArgs e)
        {
            testBtn_Click(sender, e);
        }

        Grid SimpleColorPicker()
        {
            Style btn_back = this.FindResource("btn_Background") as Style;
            Grid grid = DynamicElements.CreateGrid(2, 1, GridUnitType.Star, GridUnitType.Star);
            grid.ToolTip = "Сменить фон документа";
            Button btn = new Button();
            btn.Background = Brushes.Transparent;
            btn.Style = btn_back;
            btn.MinHeight = 20;
            DynamicElements.SetRowColumnProperties(btn, 0, 0, 1, 1);
            grid.Children.Add(btn);
            btn = new Button();
            btn.MinHeight = 20;
            btn.Style = btn_back;
            btn.Background = Brushes.White;
            DynamicElements.SetRowColumnProperties(btn, 0, 1, 1, 1);
            grid.Children.Add(btn);
            return grid;
        }

        public void SetDataSideMenu(List<LectionsModel> lectionsList) {
            Style btnStyle = this.FindResource("btn_Choice") as Style;
            ScrollViewer scroll = new ScrollViewer();
            DynamicElements.SetRowColumnProperties(scroll, 0, 0, 1, 1);
            //Grid grid = DynamicElements.CreateGrid(1, lectionsList.Count, GridUnitType.Star, GridUnitType.Star);
            StackPanel panel = new StackPanel();
            
                Style testStyle = this.FindResource("btn_Test") as Style;
                Button btn_test = new Button();
                btn_test.Name = "btn_Test";
                btn_test.Background = Brushes.Transparent;
                btn_test.Style = testStyle;
                btn_test.Content = DynamicElements.CreateViewBoxLabel("Тест по уроку", 0);
                panel.Children.Add(btn_test);

            panel.Children.Add(SimpleColorPicker());
            for (int i = 0; i < lectionsList.Count; i++)
            {
                Button btn = new Button();
                btn.Style = btnStyle;
                btn.Background = Brushes.Transparent;
                btn.Content = DynamicElements.CreateViewBoxLabel(lectionsList[i].Name, lectionsList[i].Id);
                btn.Tag = lectionsList[i].Id;
                DynamicElements.SetRowColumnProperties(btn, i, 0, 1, 1);
                //grid.Children.Add(btn);
                panel.Children.Add(btn);
            }
            scroll.Content = panel;
            mainGrid.Children.Add(scroll);
        }

        public void SetMainData(FixedDocumentSequence lection)
        {
            //FlowDocument doc = new FlowDocument();
            //doc.Background = Document.Document.Background;
            //if (!string.IsNullOrEmpty(lection.Description))
            //{
            //    Paragraph par = new Paragraph(new Run(lection.Description));
            //    doc.Blocks.Add(par);
            //}
            //Paragraph par1 = new Paragraph(new Run("TEST TEXT Present Simple обозначает действия, которые происходят в настоящее время, но не привязаны именно к моменту речи."));
            //doc.Blocks.Add(par1);
            ////Document.Document = doc;
            //Document.Document = Parse();
            Document.Document = lection;
                //CreateFile(lection.Text).GetFixedDocumentSequence();
        }

        //XpsDocument CreateFile(byte[] Content) {
        //    byte[] buffer = Content;
        //    MemoryStream newStream = new MemoryStream(buffer);
        //    var package = System.IO.Packaging.Package.Open(newStream);
        //    string inMemoryPackageName = string.Format("memorystream://{0}.xps", Guid.NewGuid());
        //    Uri packageUri = new Uri(inMemoryPackageName);
        //    System.IO.Packaging.PackageStore.AddPackage(packageUri, package);
        //    XpsDocument doc = new XpsDocument(package, System.IO.Packaging.CompressionOption.Maximum, inMemoryPackageName);
        //    return doc;
        //    //Document doc = new Document(newStream);
        //}

        public int GetNewLectionId(object sender) {
            return Convert.ToInt32((sender as Button).Tag);
        }

        public void ContentNullException(string message) {
            MessageBox.Show(message);
        }

        public void TestControlEnabled(bool enabled) {
            Button btn = LogicalTreeHelper.FindLogicalNode(mainGrid, "btn_Test") as Button;
            if (enabled) btn.Visibility = Visibility.Visible;
            else btn.Visibility = Visibility.Collapsed;
            //(btn.Parent as Panel).Children.Remove(btn);
        }

      //  FlowDocument Parse() {
      //      string str0="<FlowDocument xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"> <Paragraph FontSize=\"18\">Flow Format Example</Paragraph> <Paragraph>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure.</Paragraph>";
      //str0+="<Paragraph>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure.</Paragraph><Paragraph FontSize=\"18\">More flow elements</Paragraph><Paragraph FontSize=\"15\">Inline, font type and weight, and a List</Paragraph><List><ListItem><Paragraph>ListItem 1</Paragraph></ListItem><ListItem><Paragraph>ListItem 2</Paragraph></ListItem><ListItem><Paragraph>ListItem 3</Paragraph></ListItem><ListItem><Paragraph>ListItem 4</Paragraph></ListItem><ListItem><Paragraph>ListItem 5</Paragraph></ListItem></List>";
      //      str0+="<Paragraph><Bold>Bolded</Bold></Paragraph><Paragraph><Underline>Underlined</Underline></Paragraph><Paragraph><Bold><Underline>Bolded and Underlined</Underline></Bold></Paragraph><Paragraph><Span>The Span element, no inherent rendering</Span></Paragraph><Paragraph><Run>The Run element, no inherent rendering</Run></Paragraph>";
      //      str0 += "<Paragraph><Run Typography.Variants=\"Superscript\">This text is Superscripted.</Run> This text isn't.</Paragraph><Paragraph>      <Run Typography.Variants=\"Subscript\">This text is Subscripted.</Run> This text isn't.</Paragraph><Section><Paragraph>A block section of text</Paragraph></Section><Section BreakPageBefore=\"True\"/>    <Section><Paragraph>... and another section, preceded by a PageBreak</Paragraph></Section>    <Paragraph TextIndent=\"25\">... and this paragraph, and also the left indention.<InlineUIContainer><Border Background=\"Red\"><Label Content=\"LabelText\"/></Border></InlineUIContainer></Paragraph><Table><Table.Resources><Style TargetType=\"{x:Type TableCell}\"><Setter Property=\"BorderThickness\" Value=\"2\"/><Setter Property=\"BorderBrush\" Value=\"Black\"/></Style></Table.Resources><Table.Columns><TableColumn /><TableColumn /></Table.Columns><TableRowGroup Paragraph.TextAlignment=\"Center\"><TableRow><TableCell><Paragraph>I play</Paragraph></TableCell><TableCell><Paragraph>We play</Paragraph></TableCell></TableRow><TableRow><TableCell><Paragraph>You play</Paragraph></TableCell ><TableCell><Paragraph>They play</Paragraph></TableCell></TableRow></TableRowGroup></Table><Table><Table.Resources><Style TargetType=\"{x:Type TableCell}\"><Setter Property=\"BorderThickness\" Value=\"1\"/><Setter Property=\"BorderBrush\" Value=\"DarkBlue\"/></Style></Table.Resources><Table.Columns><TableColumn /></Table.Columns><TableRowGroup Paragraph.TextAlignment=\"Center\"><TableRow><TableCell><Paragraph>ExampleText</Paragraph></TableCell></TableRow></TableRowGroup></Table></FlowDocument>";
      //      string str = "<FlowDocument xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"> <Paragraph><Bold>Some Bold Text</Bold></Paragraph></FlowDocument>";
      //      return XamlReader.Parse(str0) as FlowDocument;
      //  }
    }
}
