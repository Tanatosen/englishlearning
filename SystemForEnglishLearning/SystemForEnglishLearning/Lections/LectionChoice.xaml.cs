﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SystemForEnglishLearning.WordLearning;

namespace SystemForEnglishLearning.Lections
{
    /// <summary>
    /// Interaction logic for LectionChoice.xaml
    /// </summary>
    public partial class LectionChoice : Window, ILectionChoiceView
    {
        public LectionChoice()
        {
            InitializeComponent();
            new LectionChoicePresenter(this, 1);
        }

        LectionChoice(double left, double top)
            : this()
        {
            this.Left = left;
            this.Top = top;
        }

        public LectionChoice(int userId, double left, double top)
            : this(left, top)
        {
            new LectionChoicePresenter(this, userId);
        }

        public LectionChoice(int userId)
        {
            new LectionChoicePresenter(this, userId);
        }

        public void SetData(List<LectionsModel> data)
        {

            List<LectionGroupModel> root = new List<LectionGroupModel>();
            data.Sort((w1, w2) => w1.Type.CompareTo(w2.Type));
            string type = "";
            foreach (LectionsModel el in data)
            {
                if (el.Type != type)
                {
                    type = el.Type;
                    root.Add(new LectionGroupModel());
                    root[root.Count - 1].Type = type;
                }
                root[root.Count - 1].Items.Add(el);
            }
            Tree.ItemsSource = root;

            //listView.ItemsSource = data;
            //CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(listView.ItemsSource);
            //PropertyGroupDescription groupDescription = new PropertyGroupDescription("Type");
            //view.GroupDescriptions.Add(groupDescription);

            //data.Add(new LectionsModel(1, "Big string test Big string test Big string test", 1, "Артикли", "", ""));
            //int row = 1;
            //int dataRow = 1;
            //RowDefinition rowDef;
            //data.Sort((w1, w2) => w1.Type.CompareTo(w2.Type));
            //string type = "";
            ////if (data[0] != null)
            ////{
            ////    type = data[0].Type;
            ////}
            ////else return;
            //Grid dataGrid = new Grid();
            //foreach (LectionsModel el in data)
            //{
            //    if (el.Type != type)
            //    {
            //        type = el.Type;
            //        Expander ex = new Expander();
            //        ex.Header = DynamicElements.CreateViewBoxLabel(el.Type,el.Id);
            //        dataGrid = new Grid();
            //        dataRow = 1;
            //        ex.Content = dataGrid;
            //        rowDef = new RowDefinition();
            //        rowDef.Height = new GridLength(1, GridUnitType.Star);
            //        grid.RowDefinitions.Add(rowDef);
            //        DynamicElements.SetRowColumnProperties(ex, row-1, 0, 1, 1);
            //        grid.Children.Add(ex);
            //        row++;
            //    }
            //    rowDef = new RowDefinition();
            //    rowDef.Height = new GridLength(1, GridUnitType.Star);
            //    dataGrid.RowDefinitions.Add(rowDef);
            //    Viewbox vb = DynamicElements.CreateViewBoxLabel(el.Name, el.Id);
            //    DynamicElements.SetRowColumnProperties(vb, dataRow - 1, 0, 1, 1);
            //    dataRow++;
            //    dataGrid.Children.Add(vb);
            //}

            //Name.DataContext = data;
            //Tree.ItemsSource = data;
            //Tree.Items.Add(data);
            //Tree.DataContext = null;
            //Tree.DataContext = data;
            ////listBox.DataContext = data;
            ////listBox.ItemsSource = data;
            ////CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(listBox.ItemsSource);
            ////PropertyGroupDescription groupDescription = new PropertyGroupDescription("Type");
            ////view.GroupDescriptions.Add(groupDescription);
        }

        public event EventHandler Item_DoubleClick = null;
        protected void HandleDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Item_DoubleClick(sender, e);
        }

        public LectionsModel GetChoosenModel(object sender) {
            LectionsModel mod = (sender as TreeViewItem).Header as LectionsModel;
            if (mod != null)
            {
                return mod;
            }
            return null;
        }

        public List<LectionsModel> GetGroupModel(object sender) {
            LectionGroupModel group = (sender as TreeViewItem).Header as LectionGroupModel;
            return new List<LectionsModel>(group.Items);
        }

        public event EventHandler Border_MouseEnter = null;
        private void Border_MouseEnter_1(object sender, MouseEventArgs e)
        {
            Border_MouseEnter(sender, e);
        }

        public event EventHandler Border_MouseLeave = null;
        private void Border_MouseLeave_1(object sender, MouseEventArgs e)
        {
            Border_MouseLeave(sender, e);
        }

    }
}
